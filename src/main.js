import Vue from 'vue'
import App from './App.vue'

// Plugins
import './plugins/bootstrap'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
